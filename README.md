# README #

Create customer 360 users!

### What is this repository for? ###

* Create customer 360 users with python script

### How do I get set up? ###

* Install the following python3 dependencies
  1. psycopg2 (postgres connection)
  2. openpyxl (Excel import/export)

* Open run.sh file and fill the respective values.
* Connect to vpn through Pritunl client
* Run sh run.sh


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Devops
* Sanjay Jain
