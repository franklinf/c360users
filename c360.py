#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import openpyxl as px
from openpyxl import load_workbook
from openpyxl.workbook import Workbook
import hashlib
import time
import random, string
import os


input_workbook          = load_workbook(filename=os.environ["INPUT_WORK_BOOK_NAME"], read_only=True)
input_worksheet         = input_workbook[os.environ["INPUT_WORK_SHEET_NAME"]]
workbook_header         = [u'emp_id', u'email_id', u'first_name', u'last_name', u'password']
output_workbook         = Workbook()
output_worksheet        = output_workbook.active
output_worksheet.title  = os.environ["OUTPUT_WORK_SHEET_NAME"]
salt                    = os.environ["SALT"]
role_id                 = os.environ["ROLL_ID"]
is_active               = os.environ["IS_ACTIVE"]
manager_id              = os.environ["MANAGER_ID"]
organization_id         = os.environ["ORGANIZATION_ID"]


output_worksheet.append(workbook_header)

def generate_password():
    return''.join(random.choices(string.ascii_letters + string.digits, k=8))

def write(query, query_data, output_data):
    try:
        post_gres_connection = psycopg2.connect(f'host={os.environ["DB_HOST"]} dbname={os.environ["DB_NAME"]} user={os.environ["DB_USER"]} password={os.environ["DB_PASSWORD"]}')
        cursor = post_gres_connection.cursor()
        cursor.execute(query, query_data)
        con.commit()
        output_worksheet.append(output_data)
    except psycopg2.DatabaseError as e:
        if post_gres_connection:
            post_gres_connection.rollback()

        print('Error %s' % e)
        sys.exit(1)

    finally:
        if post_gres_connection:
            post_gres_connection.close()
        if output_workbook:
            output_workbook.save(os.environ["OUTPUT_WORK_BOOK_NAME"])

def prepare_query(emp_id_arg, email_id_arg, first_name_arg, last_name_arg):
    print(emp_id_arg, email_id_arg, first_name_arg, last_name_arg)

    if emp_id_arg is None and email_id_arg is None:
        print('could not insert data to db, emp_id and email_id is none.')
        return

    if first_name_arg is None or len(first_name_arg) > 19:
        print('could not insert data to db, first_name is none.', first_name_arg)
        return

    if last_name_arg is None or len(first_name_arg) > 19:
        print('could not insert data to db, last_name is none.', last_name_arg)
        return

    if type(emp_id_arg) is float:
        emp_id_arg = int(emp_id_arg)

    if emp_id_arg is None :
        emp_id_arg = email_id_arg.split('@')[0]

    if email_id_arg is None:
        email_id = str(emp_id_arg) + os.environ["EMAIL_SUFFIX"]
        return

    password = generate_password();

    secret_hash = hashlib.sha512(str(salt+password).encode('utf-8')).hexdigest()
    query = "INSERT into users (nt_id,email_id,first_name,last_name,secret_key_hash,salt,role_id,is_active,manager_id,organization_id, created_at, updated_at) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    query_data = (emp_id_arg, email_id_arg, first_name_arg, last_name_arg, secret_hash, salt, role_id, is_active, manager_id, organization_id, int(time.time()), int(time.time()))
    # output data required for writing it to excel
    output_data = [emp_id_arg, email_id_arg, first_name_arg, last_name_arg, password]
    write(query, query_data, output_data)

row_index = 0
for row in input_worksheet.rows:
    if row_index != 0:
        prepare_query(row[0].value, row[1].value, row[2].value, row[3].value)
    row_index += 1
